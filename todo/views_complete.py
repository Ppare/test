from django.shortcuts import redirect, get_object_or_404

from .models import Task


def complete_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    task.is_complete = True
    task.save(update_fields=['is_complete', 'datetime_complete'])
    return redirect('todo:home')
