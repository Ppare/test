from django import forms


class AddTask(forms.Form):
    name = forms.CharField(label='task_name', max_length=256)
