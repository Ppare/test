from django.urls import path

from .views import home_view
from .views_complete import complete_view
from .views_delete import delete_view

app_name = 'todo'
urlpatterns = [
    path('', home_view, name='home'),
    path('<int:task_id>/complete/', complete_view, name='complete'),
    path('<int:task_id>/delete/', delete_view, name='delete'),
]
