from django.shortcuts import render
from .models import Task
from .forms import AddTask


def home_view(request):
    complete_task_list = Task.objects.filter(is_complete=True)
    incomplete_task_list = Task.objects.filter(is_complete=False)
    if request.method == 'POST':
        form = AddTask(request.POST, request.FILES)
        if form.is_valid():
            name = form.cleaned_data['name']
            add_task = Task(name=name)
            add_task.save()
        else:
            form = AddTask()

    return render(
        request,
        'home.html',
        {
            'complete_task_list': complete_task_list,
            'incomplete_task_list': incomplete_task_list,
        }
    )
