from django.db import models


class Task(models.Model):
    name = models.CharField(max_length=256)
    is_complete = models.BooleanField(default=False)

    datetime_complete = models.DateTimeField(null=True, blank=True)
    datetime_create = models.DateTimeField(auto_now_add=True)
    datetime_update = models.DateTimeField(auto_now=True)
