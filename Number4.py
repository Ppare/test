input_fac = int(input())

ans = 0
fac = input_fac
for i in range(1, input_fac):
    fac *= i
print(fac)

fac_list = list(str(fac))
fac_list.reverse()

count = 0
for fac in fac_list:
    if fac == '0':
        count += 1
    else:
        break
print(count)
